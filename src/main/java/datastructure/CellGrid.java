package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private final int rows;
    private final int cols;
    private final CellState[][] grid;


    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows=rows;
        this.cols=columns;
        grid = new CellState[rows][columns];

    }

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }
    //true if the value is withing limits false if not
    public boolean limittest(int row, int column) {
        return row >= 0 && row < numRows() && column >= 0 && column < numColumns();
    }


    @Override
    public void set(int row, int column, CellState element) {
        if (limittest(row, column)) {
            grid[row][column] = element;
        } else {
            throw new IndexOutOfBoundsException();
        }


    }

    @Override
    public CellState get(int row, int column) {
        if (limittest(row, column)) {
            return grid[row][column];
        } else {
            throw new IndexOutOfBoundsException();
    }}

    @Override
    public IGrid copy() {
        IGrid CellgridCopy = new CellGrid(numRows(),numColumns(),CellState.DEAD);
        for (int row = 0; row < numRows(); row++) {
            for (int column = 0; column < numColumns(); column++) {
                CellgridCopy.set(row, column, get(row, column));
            }
        }
        return CellgridCopy;
    }


}
