package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton{

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }



    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }
    }

    @Override
    public void step() {
        IGrid nextGen = currentGeneration.copy();
        for(int r=0; r<nextGen.numRows();r++) {
            for(int c=0; c<nextGen.numColumns(); c++) {
                nextGen.set(r, c, getNextCell(r, c));
            }

        }
        this.currentGeneration = nextGen;
    }

    @Override
    public CellState getNextCell(int row, int col) {
        if(this.currentGeneration.get(row, col).equals(CellState.ALIVE)) {
            return CellState.DYING;
        }else if(this.currentGeneration.get(row, col).equals(CellState.DYING)){
            return CellState.DEAD;
        }else if(this.currentGeneration.get(row, col).equals(CellState.DEAD)) {
            if(countNeighbors(row,col,CellState.ALIVE)==2) {
                return CellState.ALIVE;
            }
        }
        return CellState.DEAD;
    }

    private int countNeighbors(int row, int col, CellState state) {

        int count = 0;

        for (int r = row-1; r <= row+1; r++){
            for (int c = col-1; c<=col+1; c++){
                if(c >= numberOfColumns()){
                    continue;
                }
                else if(c < 0){
                    continue;
                }
                else if(r >= numberOfRows()){
                    continue;
                }
                else if(r < 0){
                    continue;
                }
                else if(c==col && r==row){
                    continue;
                }
                if (currentGeneration.get(r, c) == state){
                    count++;
                }
            }
        }
        return count;
    }



    @Override
    public CellState getCellState(int row, int col) {
        return currentGeneration.get(row, col);
    }

    @Override
    public int numberOfRows() {
        return this.currentGeneration.numRows();
    }



    @Override
    public int numberOfColumns() {
        return  this.currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }

}
